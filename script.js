let playerOne;
let playerTwo;
let count = 0;
let classPlayer;
let verifyH;
let verifyV;
let verifyD;
let drop = true
let display = document.querySelector("#display");
let modalReset = document.getElementById('modal')



const choicePlayer = () => {
    
    let className = '';
    if(drop) {
        if(count % 2 == 0){
            className = 'playerOne';
        }
        else{
            className = 'playerTwo';
        }
    }
    
    count++;
    return className;
}
const modal = ()=>{
    if(drop) {
        const destinyDiv = document.getElementById('modal');
        let modalDiv = document.createElement('div');
        modalDiv.classList.add('modalDiv');
        let title = document.createElement('h2');
        title.classList.add('titleModal');
        let message = document.createTextNode(`${classPlayer} Ganhou!`);
        title.appendChild(message);
        modalDiv.appendChild(title);
        destinyDiv.appendChild(modalDiv);
        drop = false
    }
}

let arrMonster = [
    [' ', ' ', ' ', ' ', ' ', ' ',],
    [' ', ' ', ' ', ' ', ' ', ' ',],
    [' ', ' ', ' ', ' ', ' ', ' ',],
    [' ', ' ', ' ', ' ', ' ', ' ',],
    [' ', ' ', ' ', ' ', ' ', ' ',],
    [' ', ' ', ' ', ' ', ' ', ' ',],
    [' ', ' ', ' ', ' ', ' ', ' ',]
]
const columGame = (colum, i) => {
    const filho = document.createElement('div')
    filho.classList.add('filho');
    classPlayer = choicePlayer();
    if(classPlayer ){
        filho.classList.add(classPlayer);
    }
    if(colum.childElementCount < 6 && drop){                    
        colum.appendChild(filho);
       }
       let transform = [...colum.childNodes]
       if(transform[transform.length -1]){
           arrMonster[i][transform.length -1] = transform[transform.length -1].className.slice(-1)                
       }
       vertical()
       horizontal()
       diagonal()
       diagonalInverse()   
}

const createRenderDiv = () => {
    
     for (let i = 0; i < 7; i++){  
         let colum = document.createElement('div');
         colum.id = (`colum${i}`);
         colum.classList.add('celPlay');
         display.appendChild(colum);
         
         colum.addEventListener('click', () => {
                columGame(colum, i)
         })     
    }
}


createRenderDiv()


const vertical = ()=>{
    for (let i = 0; i < arrMonster.length; i++) {
        let line = ""
        for (let j = 0; j < arrMonster[i].length; j++) {
            line += arrMonster[i][j]
        }
        if(line.includes("eeee")) {
            modal()
        }
        if(line.includes("oooo")){
            modal()
        }
    }
}

const horizontal = ()=>{
    for (let i = 0; i < arrMonster.length; i++) {
        let line = ""
        for (let j = 0; j < arrMonster[i].length; j++) {
            if(arrMonster[j][i]) {
                line += arrMonster[j][i]
            }
        }
        if(line.includes("eeee")) {
            modal()
        }
        if(line.includes("oooo")){
            modal()
        }
    }
}

const diagonal = ()=>{

    for (let i = 0; i < arrMonster.length; i++) {
        for (let j = 0; j < 3; j++) {

            let celula = arrMonster[j][i]
            if (arrMonster[j+3]) {
                if(celula === " " || arrMonster[j + 1][i + 1] === " " || arrMonster[j + 2][i + 2] === " " || arrMonster[j + 3][i + 3] === " ") {
                    break
                }
                if(celula === arrMonster[j + 1][i + 1] && 
                    celula === arrMonster[j + 2][i + 2] &&
                    celula === arrMonster[j + 3][i + 3] ) {
                        
                        if(celula === "o"){
                            modal()
                        }
                        if(celula === "e") {
                            modal()
                    }
                }
            }
        }
    }
}
    
const diagonalInverse = ()=>{
    for (let i = 0; i < arrMonster.length; i++) {

        for (let k = 6; k >= 3; k--) {
            let celula = arrMonster[k][i]
            if (arrMonster[k-3]) {
                if(celula === arrMonster[k - 1][i + 1] && 
                    celula === arrMonster[k - 2][i + 2] &&
                    celula === arrMonster[k - 3][i + 3] ) {

                        if(celula === "o"){
                            modal()
                            console.log('playertwo')
                        }
                        if(celula === "e") {
                            modal()
                            console.log('playerone')
                        }
                }
            }
        }
    }
}
 
const reset = () => {
    display.innerHTML = ""
    modalReset.innerHTML = ""
    arrMonster = [
        [' ', ' ', ' ', ' ', ' ', ' ',],
        [' ', ' ', ' ', ' ', ' ', ' ',],
        [' ', ' ', ' ', ' ', ' ', ' ',],
        [' ', ' ', ' ', ' ', ' ', ' ',],
        [' ', ' ', ' ', ' ', ' ', ' ',],
        [' ', ' ', ' ', ' ', ' ', ' ',],
        [' ', ' ', ' ', ' ', ' ', ' ',]
    ]
    drop = true
    createRenderDiv()
}

const button = document.getElementById("buttonOrganize")
button.addEventListener("click", reset)
